import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_bvp

def cheby_points(a,b,n):
	return np.sort(.5*(a+b + (b-a)*np.cos(np.pi*((2.*np.arange(1,n+1)-1)/(2*n)))))

nvars = 6
w, wz, theta, thetaz, theta_bar,theta_barz = range(nvars)

def ekman_initial(Ra_hat, eps, k = 1, fname = "guess.npz"):
	def bc(y_0,y_1):
		return np.array([(k**2*y_0[w]-(eps/2)**.5*y_0[wz])**2, y_0[theta]**2, (y_0[theta_bar]-1)**2, (k**2*y_1[w]+(eps/2)**.5*y_1[wz])**2, y_1[theta]**2, (y_1[theta_bar])**2])

	def f(z, y):
		res = np.zeros((nvars,len(y[0])))
		res[theta] = y[thetaz]
		res[w] = y[wz]
		res[theta_bar] = y[theta_barz]
		res[wz] = k**6*y[w] - k**4*Ra_hat * theta
		res[thetaz] = k**2*y[theta] + eps**-2 * (y[w] * y[theta_barz])
		res[theta_barz] = y[wz]*y[theta] + y[thetaz]*y[w]
		return res

	n = 100
	dom = np.hstack([[0], cheby_points(0,1,n-2),[1]])
#	dom = np.linspace(0,1,100)
	
	guess = np.zeros((nvars, n))
	guess[theta_bar] = 1-dom
	guess[theta] = np.sin(2*np.pi*dom)
	guess[theta_barz] = -1
	guess[thetaz] = 2*np.pi * np.cos(2*np.pi*dom)
	guess[w] = (.5-dom)**3 #make it negative at the top and positive at the bottom
	guess[wz] = 3*(dom-.5)**2
	res = solve_bvp(f,bc, dom,guess, max_nodes=10000, verbose=2)
	print(res.status)
#	plt.plot(res.x, res.y[w], label='w')
#	plt.plot(res.x, res.y[theta_bar], label='theta_bar')
#	plt.plot(res.x, res.y[wz], label='$w_z$')
#	plt.legend()
#	plt.show()
#	plt.plot(res.x, res.y[theta], label='theta')
#	plt.title("$\\theta$")
#	plt.show()
#	plt.hist(res.x)
#	plt.show()
	np.savez(fname, c=res.sol.c, x = res.sol.x)
#	print res.sol.c, res.sol.x
	print(res.sol(dom))

if __name__ == "__main__":
	ekman_initial(9.9,.0046)
