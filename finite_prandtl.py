import numpy as np
from dedalus import public as de
import argparse
import os
from dedalus.extras import flow_tools
import initial_guess as ig
from scipy.interpolate import PPoly
from mpi4py import MPI

class simulator:
	def __init__(self, prandtl=1, Ra=1e10, Ek=1e-6, hsize=100, vsize=100, L=4, Ra_hat=None,guess=None):
		"""
		A class for simulating Ekman Pumping at finite Prandtl numbers
		
		Parameters:
			prandtl (float): The Prandtl number
			Ra (float): The Raleigh number
			Ek (float): The Ekman number
			hsize: The number of points to use in each horizontal dimension
			vsize: the number of points to use in the vertical direction
			guess: An npz archive with the initial state
		"""
		self.guess = guess
		if Ra_hat is None: Ra_hat = Ra*Ek**(4./3)
		eps = Ek**(1./3)
		print ("Ra_hat {}, eps {}".format(Ra_hat, eps))
		
		#setup the problem
		x_basis = de.Fourier('x', hsize, interval = (-L/2,L/2),dealias=3./2)
		y_basis = de.Fourier('y', hsize, interval=(-L/2,L/2), dealias=3./2)
		z_basis = de.Chebyshev('z', vsize, interval=(0,1), dealias=3./2)

		domain = de.Domain([x_basis, y_basis, z_basis], grid_dtype=np.float64)
		
		#Variables where we need to take second derivative in the vertical direction
		vert_vars = {'T_bar','theta'}
		slack_vars = {'wtheta_flux','theta','psi','w'}
		base_vars = ['psi','w','theta','T_bar','phi']
		#psi is pressure, zeta is the vertical component of vorticity
		#w is the vertical component of velocity
		#theta is the tempurature deviation. T_bar is the horizontal mean
		#So real tempurature = theta + T_bar
		#psi, zeta, theta and w should all have zero horizontal mean
		for var in vert_vars:
			base_vars.append(var+'z')
		#for var in slack_vars:
		#	base_vars.append(var+'_slack')
		#wtheta_fluz is (w theta)', or the fluxuation about the average
		problem = de.IVP(domain, variables=base_vars)
		
		#Horizontal laplacian
		problem.substitutions['LapH(A)'] = "dx(dx(A)) + dy(dy(A))"
		#Jacobian operator
		problem.substitutions["Jac(A)"] = "dx(psi)*dy(A) - dy(psi)*dx(A)"
		problem.substitutions["zeta"] = "LapH(psi)"
		problem.substitutions["wtheta_bar"] = "integ(w*theta,'x','y')/L**2"
		problem.substitutions["wtheta_flux"] = "w*theta - integ(w*theta,'x','y')/L**2"

		problem.parameters["Ra_hat"] = Ra_hat
		problem.parameters["Pr"] = prandtl
		problem.parameters["eps"] = eps
		problem.parameters["L"] = L

		problem.add_equation("dt(zeta) - dz(w) - LapH(zeta) = -Jac(zeta)", condition="(nx!=0) or (ny!=0)")
		problem.add_equation("psi=0", condition="nx==0 and ny == 0")
#		problem.add_equation("thetaz=0", condition="nx==0 and ny == 0")
		problem.add_equation("dt(w) + dz(psi) - (Ra_hat/Pr)*theta - LapH(w) = -Jac(w)")
#		problem.add_equation("w=0", condition="nx==0 and ny==0")
		problem.add_equation("dt(theta) - (1/Pr) * (LapH(theta) + eps*eps*dz(thetaz)) = -Jac(theta) - w*dz(T_bar) - eps*(dx(dx(phi)*theta) + dy(dy(phi)*theta)) - eps*dz(wtheta_flux)")
#		problem.add_equation("theta=0")
		problem.add_equation("dt(T_bar)/eps - (eps/Pr)*dz(T_barz) = -eps*dz(wtheta_bar)")
#		problem.add_equation("T_bar=0", condition="nx!=0 or ny!=0")
		#Now try to account for the bars, the correction terms, and so on
		problem.add_equation("LapH(phi) + dz(w) = 0", condition="nx!=0 or ny != 0")
		problem.add_equation("phi = 0", condition="nx==0 and ny == 0")
		#Irrotationality
		#problem.add_equation("dx(u2)-dy(u1) = 0",condition="(nx != 0) or (ny != 0)")
		#problem.add_equation("u2=0",condition="(nx == 0) and (ny == 0)")
		#problem.add_equation("u1=0",condition="(nx == 0) and (ny == 0)")


		#add derivative reduction equations Only needed for the Chebyshev basis
#		problem.add_equation("T_barz-dz(T_bar)=0", condition="nx==0 and ny==0")
#		problem.add_equation("thetaz-dz(theta)=0", condition="nx!=0 or ny!=0")
		for var in vert_vars:
			problem.add_equation("{} - dz({}) = 0".format(var+'z', var))

		problem.add_bc("left(theta) = 0")
		problem.add_bc("left(T_bar) = 1")
		problem.add_bc("left(sqrt(eps/2)*zeta-w) = 0")
		problem.add_bc("right(theta) = 0")
		problem.add_bc("right(T_bar) = 0")
		problem.add_bc("right(sqrt(eps/2)*zeta+w) = 0", condition="nx!=0 or ny != 0")
		problem.add_bc("left(LapH(phi)-sqrt(2/eps)*dz(phi)) = 0", condition="nx!=0 or ny!=0")
#		problem.add_bc("left(phi-sqrt(2/eps)*w) = 0", condition="nx!=0 or ny!=0")
		self.problem = problem
		self.domain = domain
		self.pr = prandtl
		self.L = L

	def initialize(self, solver):
		x,y,z = self.domain.grids(scales=1)
		
		if self.guess is None:
			solver.state['T_bar']['g'] = 1-z
			solver.state['T_bar'].differentiate('z', out=solver.state['T_barz'])
			solver.state['w']['g'] = z-.5
			return

		ark = np.load(self.guess)
		sol = PPoly(ark['c'], ark['x'])(z)
		solver.state['T_bar']['g'] = sol[:,:,:,ig.theta_bar]
		solver.state['T_barz']['g'] = sol[:,:,:,ig.theta_barz]
		#A planform
		k = ark['k']
		h = (2**.5)*np.cos(k*x)
#		solver.state['T_bar'].differentiate('z', out=solver.state['T_barz'])
		from matplotlib import pyplot as plt
#		plt.plot(z.flatten(), sol[:,:,:,ig.theta_barz].flatten())
#		plt.show()
		solver.state['w']['g'] = h * sol[:,:,:,ig.w] / self.pr
		solver.state['theta']['g'] = h * sol[:,:,:,ig.theta]
		solver.state['theta'].differentiate('z', out=solver.state['thetaz'])
		solver.state['psi']['g'] = -h * sol[:,:,:,ig.wz] / (k**4*self.pr)
		print("Completed initialization")


	def solve(self, record_dir, max_iterations=2500, sim_time=.5):	
		#How to add initial conditions?
		solver = self.problem.build_solver(de.timesteppers.RK443)
		self.initialize(solver)
		solver.stop_iteration = max_iterations
		solver.stop_wall_time = np.inf
		solver.stop_sim_time = sim_time
		dt = .5*1e-4
		annals = solver.evaluator.add_file_handler(
                                    os.path.join(record_dir, "analysis"),
                                    iter=5, max_writes=73600, mode="append")
		annals.add_task("integ(T_barz**2 + dx(theta)**2+dy(theta)**2 + (eps*thetaz)**2, 'x','y','z')/L**2", name="Nu")
		annals.add_task("integ(dx(theta)**2+dy(theta)**2, 'x','y','z')/L**2", name="theta_horiz")
		annals.add_task("integ((eps*thetaz)**2, 'x','y','z')/L**2", name="theta_vert")
		annals.add_task("integ(T_barz**2, 'x','y','z')/L**2", name="T_barz")
		annals.add_task("integ(w**2, 'x','y','z')/L**2", name="w_norm")

		cfl = flow_tools.CFL(solver, initial_dt=dt, cadence=1, safety=1,
                                            max_change=1.5, min_change=0.1,
                                            max_dt=0.001,    min_dt=1e-8)
		cfl.add_velocities(('dx(phi)', 'dy(phi)', 'w' ))
		while solver.ok:
			dt = cfl.compute_dt()
			dt = solver.step(dt)
			if (solver.iteration % 10) == 0:
				print("Completed iteration {}, sim time {}, dt {}".format(solver.iteration, solver.sim_time, dt))

def do_merge(dirname,task_name="analysis"):
	import pathlib
	from dedalus.tools import post
	post.merge_process_files(dirname, cleanup=True)
	set_paths = list(pathlib.Path(dirname).glob(task_name+"_s*.h5"))

	if len(set_paths): post.merge_sets(os.path.join(dirname,task_name+".h5"), set_paths, cleanup=True)
	

def plot_nusselt(record_dir):
	import matplotlib.pyplot as plt
	import h5py
	dirname = os.path.join(record_dir,"analysis")
	do_merge(dirname)
	print(dirname)
	with h5py.File(dirname+"/analysis.h5", 'r') as f:
		nu = f['tasks']['Nu']
		print(np.array(nu).flatten())
		print(np.array(f['tasks']['theta_horiz']).flatten())
		print(np.array(f['tasks']['theta_vert']).flatten())
		print(np.array(f['tasks']['T_barz']).flatten())
		print(np.array(f['tasks']['w_norm']).flatten())

#		plt.plot(nu)
#		plt.show()

if __name__ == "__main__":
	print ("Finite-Prandtl simulation of Ekman Pumping")
	record_dir = "prandtl_data"

	parser = argparse.ArgumentParser()
	parser.add_argument("mode")
	parser.add_argument("--hsize", type=int, default=16)
	parser.add_argument("--vsize", type=int, default=20)

	args = parser.parse_args()
	if args.mode == "solve":
		sim = simulator(hsize=args.hsize, vsize=args.vsize, L=10*4.8154, Ra_hat=10, Ek=1e-7, prandtl=1, guess="guess.npz")
		sim.solve(record_dir)
	elif args.mode == "analyze":
		plot_nusselt(record_dir)

